#include "task_manager/scheduler.hpp"

#include <iostream>

using std::thread;

namespace task_manager
{
    Scheduler::Scheduler()
    {
        defaults = time::now( time::millisecond_t( 60 ) );
        runner = thread( [this] { run(); } );
        runner.detach();
    }

    Scheduler::~Scheduler()
    {
        join();
    }

    void Scheduler::sched( const Task::Callback_t & callback, long int offset_ms )
    {
        sched( callback, time::millisecond_t( offset_ms ) );
    }

    void Scheduler::sched( const Task::Callback_t & callback, time::millisecond_t offset )
    {
        sched( callback, time::now( offset ) );
    }

    void Scheduler::sched( const Task::Callback_t & callback, time::ptime expiration )
    {
        Task *task = new Task( callback, expiration );
        mtx.lock();
        bool was_empty = tasks.empty();
        tasks.push( task );
        if ( ! was_empty )
        {
            if ( task == tasks.top() )
            {
                worker.changed_work_time();
            }
        }
        mtx.unlock();
    }

    void Scheduler::join()
    {
        if ( runner.joinable() )
            runner.join();
    }

    time::ptime Scheduler::first_timeout()
    {
        time::ptime expiration;
        mtx.lock();
        if ( tasks.empty() )
        {
            expiration = defaults;
        }
        else
        {
            expiration = tasks.top()->expiration();
        }
        mtx.unlock();
        return expiration;
    }

    bool Scheduler::empty()
    {   
        mtx.lock();
        bool empty = tasks.empty();
        mtx.unlock();
        return empty;
    }

    Task * Scheduler::top()
    {   
        mtx.lock();
        Task * task = tasks.top();
        tasks.pop();
        mtx.unlock();
        return task;
    }

    void Scheduler::push( Task * task )
    {   
        mtx.lock();
        tasks.push( task );
        mtx.unlock();
    }

    void Scheduler::run()
    {
        while ( true )
        {   
            time::ptime wake_up = first_timeout();
            bool work = worker.rest_until_work( wake_up );
            if ( work )
            {
                process_expired_tasks();
            }
        }
    }

    void Scheduler::process_expired_tasks()
    {
        time::ptime now = time::now();
        while( ! empty() )
        {
            Task * task = top();
            if( task->expired( now ) )
            {
#ifdef DEBUG
                std::cout
                    << time::to_string()
                    << ( long ) task
                    << "  "
                    << task->to_string()
                    << std::endl;
#endif
                task->execute();
                delete task;
            }
            else
            {
                push( task );
                break;
            }
        }
    }
}// namespace task_manager