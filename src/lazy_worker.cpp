#include "task_manager/lazy_worker.hpp"

#include <thread>

using std::unique_lock;
using std::mutex;

namespace task_manager
{
    Lazy_Worker::Lazy_Worker()
    {
        lock = unique_lock< mutex >( cv_m );
    }

    void Lazy_Worker::changed_work_time()
    {
        changed = true;
        cv.notify_all();
    }

    bool Lazy_Worker::rest_until_work( time::ptime wake_time )
    {   
        changed = false;
        if( cv.wait_until( lock, wake_time, [ this ](){return changed == true;} ) )
        {
            return false;
        }
        else
        {
            return true;
        }
    }

}
