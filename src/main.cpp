// #include <string>
#include <random>
#include <functional>
#include <sstream>
#include <iostream>
#include <thread>

#include <unistd.h>

#include "task_manager/task_manager.inc"

struct MyFunctor
{
    void operator() () const
    {
        std::cout
            << "Ryuuuu se fez um functor"
            << "["
            << this->n
            << "]"
            << std::endl;
    }

    int n;
};

void print( const std::string &to_print )
{
    std::cout
        << to_print
        << std::endl;
}

int main ()
{
    //Rand
    std::random_device rd;  //Will be used to obtain a seed for the random number engine
    std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
    std::uniform_int_distribution<> dis(3000, 10000);
    //Rand

    task_manager::Scheduler sc;
    
    for ( int n = 0; n < ( 1000 * 1000 ) / 3; ++n )//* 1000
    {
        sc.sched(std::bind( print, "da hadouken ryuuuu, ele e mauuuu" ),  dis(gen) );
        
        sc.sched( MyFunctor{ n }, dis(gen) );
        
        sc.sched
        (
            []() {
            std::cout
                << "da hadouken ryuuuu, ele e bandiduu"
                << std::endl;
            }
        );
    }

    while ( ! sc.empty() ) {}
  return 0;
}
