#include "task_manager/time.hpp"

#include <string>

namespace task_manager
{
namespace time
{	
    ptime start_time = task_clock::now();

    ptime now( millisecond_t offset )
	{
	    return task_clock::now() + offset;
    }

    std::string to_string( ptime t )
    {
        return std::to_string( ( t - start_time ).count() );
    }
    
    std::string to_string()
	{
	    return to_string( now() );
    }
}// namespace task_manager::time
}// namespace task_manager