# Task Manager
A simple manager for async callbacks

## dependency
* cmake >= 3.1
* pthread
* compiler c++11 support

## build
* mkdir build
* cd build
* cmake -DCMAKE_BUILD_TYPE=Release ..
* cmake --build .
* The build will create a sampler binary program :) and a static library to be used
