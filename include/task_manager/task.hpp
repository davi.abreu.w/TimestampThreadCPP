#pragma once

#include <functional>
#include <string>

#include "time.hpp"

namespace task_manager
{
    class Task
    {
        public:
            typedef std::function< void ( void ) > Callback_t;

            Task( const Callback_t & callback, time::ptime offset );

            bool lesser_priority_than( const Task & other ) const;

            bool higher_priority_than( const Task & other ) const;

            bool operator < ( const Task & other ) const;

            bool operator > ( const Task & other ) const;
            
            bool expired( time::ptime now ) const;

            void execute();

            time::ptime expiration();

            std::string to_string() const;

        private:
            const time::ptime execute_at;
            Callback_t callback;
    };

    class TaskPointerPriorityComparator
    {
        public:
            bool operator()( const Task *first, const Task *second ) const
            {
                return ( * first ) < ( * second );
            }
    };
}// namespace task_manager