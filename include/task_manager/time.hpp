#pragma once

#include <chrono>
#include <string>

namespace task_manager
{
namespace time
{	
	typedef std::chrono::steady_clock task_clock;
    typedef std::chrono::steady_clock::time_point ptime;
    typedef std::chrono::milliseconds millisecond_t;

    ptime now( millisecond_t offset = millisecond_t( 0 ) );
    std::string to_string( ptime t );
    std::string to_string();

}// namespace task_manager::time
}// namespace task_manager