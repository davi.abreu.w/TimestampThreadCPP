#pragma once

#include <atomic>
#include <condition_variable>

#include "time.hpp"

namespace task_manager
{
    class Lazy_Worker
    {
        public:
            Lazy_Worker();
            void changed_work_time();
            bool rest_until_work( time::ptime wake_time );
        private:
            std::mutex cv_m;
            std::unique_lock<std::mutex> lock;
            std::condition_variable cv;
            std::atomic< int > changed{ 0 };
    };
}