#pragma once

#include <queue>
#include <vector>
#include <mutex> 
#include <thread>

#include "task.hpp"
#include "time.hpp"
#include "lazy_worker.hpp"

namespace task_manager
{   
    class Scheduler
    {
        public:
            Scheduler();
            ~Scheduler();
            void sched( const Task::Callback_t &, long offset_ms );
            void sched( const Task::Callback_t &, time::millisecond_t );
            void sched( const Task::Callback_t &, time::ptime = time::now() );
            bool empty();

        private:
            typedef std::priority_queue< Task *, std::vector< Task * >, TaskPointerPriorityComparator > Tasks;
            Tasks tasks;
            std::mutex mtx;
            Lazy_Worker worker;
            std::thread runner;
            time::ptime defaults;
            time::ptime first_timeout();
            void process_expired_tasks();
            Task * top();
            void push( Task * task );
            void run();
            void join();
    };
}// namespace task_manager
